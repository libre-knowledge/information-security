# 資訊安全<br>Information security

確保資訊系統不會受到未經授權的第三方破壞

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white "本專案使用 pre-commit 檢查專案中的潛在問題")](https://github.com/pre-commit/pre-commit) [![REUSE 規範遵從狀態標章](https://api.reuse.software/badge/gitlab.com/libre-knowledge/information-security "本專案遵從 REUSE 規範降低軟體授權合規成本")](https://api.reuse.software/info/gitlab.com/libre-knowledge/information-security)

## 參考資料

* [資訊安全 - 維基百科，自由的百科全書](https://zh.wikipedia.org/wiki/%E4%BF%A1%E6%81%AF%E5%AE%89%E5%85%A8)
  [Information security - Wikipedia](https://en.wikipedia.org/wiki/Information_security)
  維基百科條目
* [Computer security - Wikipedia](https://en.wikipedia.org/wiki/Computer_security)

---

本主題為[自由知識協作平台](https://libre-knowledge.github.io/)的一部分，除部份特別標註之經合理使用(fair use)原則使用的內容外允許公眾於授權範圍內自由使用

如有任何問題，歡迎於本主題的[議題追蹤系統](https://github.com/libre-knowledge/_專案ID_/issues)創建新議題反饋
